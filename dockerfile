FROM node:16.10.0

WORKDIR /app

COPY package.json /app/
COPY yarn.lock /app/

RUN npm install

COPY index.js /app/index.js