# Local DynamoDB

Just a bundle for easy setup when using a local DynamoDB instance.

## 🐳 Installation

You will need [docker](https://www.docker.com/) (and docker-compose) for this project. That's it.

## Usage

If you have [yarn](https://yarnpkg.com/):

```sh
yarn start
```

or, if you don't have yarn:

```sh
docker-compose build && docker-compose up
```

Then, you can access your dynamo admin in your browser through http://localhost:8001

> Port number 8001 is for you dynamo admin GUI. For your database, the port number is 8000.

---

## ⚠️ Atention

AWS SDK uses a preset dynamoDB endpoint. For you to use it locally, you have to change the endpoint when you invoke dynamo.

If you do it through `awscli`, you just have to add the endpoint at the end of all your dynamo related commands.

```sh
aws dynamodb list-tables --endpoint-url http://localhost:8000
```

If you do it through your code, you have to create a `AWS.Endpoint` and pass it to your dynamo instance.

```js
import { DynamoDB, Endpoint } from 'aws-sdk';

const dynamoDB = new DynamoDB({ apiVersion: '2012-08-10' });

dynamoDB.endpoint = new Endpoint('http://localhost:8000');

// now you can use dynamo...
```
