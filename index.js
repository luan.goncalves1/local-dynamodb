const AWS = require('aws-sdk');
const { createServer } = require('dynamodb-admin');

const dynamodb = new AWS.DynamoDB({ endpoint: process.env.AWS_DYNAMO_ENDPOINT });
const dynClient = new AWS.DynamoDB.DocumentClient({ service: dynamodb });

const app = createServer(dynamodb, dynClient);

const port = 8001;
const server = app.listen(port);
server.on('listening', () => {
  const address = server.address();
  console.log(`Listening on port ${address.port}`);
});
